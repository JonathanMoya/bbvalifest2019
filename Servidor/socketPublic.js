let personasAyuda = [];

module.exports = function (socket, publicSocket, io, bd, equipos, pdf) {

    function getAllComputer() {
        let algo = io.of('/public').sockets;
        let ip = [];
        Object.keys(algo).forEach(function (el) {
            let actuallIp = algo[el].handshake.address.split('::ffff:')[1];
            if (!ip.includes(actuallIp)) {
                ip.push(actuallIp);
            }
        });
        return ip;
    }

    publicSocket.emit('getAllSockets', {cantidad: getAllComputer(), ayuda: personasAyuda, equipos: equipos});

    bd.getDB(function (data) {
        socket.emit('personasAll', data);
        publicSocket.emit("actualizarEquipos", {"equipos": equipos});
    });

    socket.on('deletePerson', function (cc) {
        bd.deletePerson(cc, function (callback) {
            console.log(callback);
            bd.getDB(function (data) {
                publicSocket.emit('personasAll', data);
            });
        })
    });

    socket.on('undoRegistrar', function (cc) {
        bd.undoRegistrar(cc, function (callback) {
            console.log(callback);
            bd.getDB(function (data) {
                publicSocket.emit('personasAll', data);
            });
        })
    });

    socket.on('getAllSockets', function () {
        publicSocket.emit('getAllSockets', {cantidad: getAllComputer(), ayuda: personasAyuda, equipos: equipos});
    });

    socket.on('registrarPersona', function (persona) {
        console.log(persona);
        bd.registrarPersona(persona.persona.cc, function (callback) {
            console.log(persona.cc);
            bd.getDB(function (data) {
                publicSocket.emit('personasAll', data);
            });
        })
    });

    socket.on('imprimirPDF', function (datos) {
        pdf.crearPDF(datos.persona, function () {
            if (datos.impresora === "0") {
                socket.emit('imprimirPDF', datos.persona);
            } else {
                console.log("datos");
                console.log(datos);
                publicSocket.emit('imprimirPDFAliado', datos);
            }
        });
    });

    socket.on('newPerson', function (data) {
        bd.buscarPersonaCC(data.cc, function (call) {
            if (call.length === 0) {
                bd.newPerson(data, function (callbak) {
                    bd.registrarPersona(data.cc, function (callback) {
                        socket.emit('responseNewPerson', {res: callbak});
                        bd.getDB(function (data) {
                            publicSocket.emit('personasAll', data);
                        });
                    });
                })
            } else {
                socket.emit('responseNewPerson', {res: 'CC Duplicated'});
            }
        });
    });

    socket.on('pedirAyuda', function () {
        personasAyuda.push(socket.handshake.address.split('::ffff:')[1]);
        publicSocket.emit('getAllSockets', {cantidad: getAllComputer(), ayuda: personasAyuda});
    });

    socket.on('terminarAyuda', function (socketIp) {
        if (personasAyuda.includes(socketIp)) {
            for (var i = 0; i < personasAyuda.length; i++) {
                if (personasAyuda[i] === socketIp) {
                    personasAyuda.splice(i, 1);
                    i--;
                }
            }
        }
        publicSocket.emit('getAllSockets', {cantidad: getAllComputer(), ayuda: personasAyuda, equipos: equipos});
    });


    //Host names

    socket.on('ingresarEquipo', function (info) {
        //verificar que no exista

        let existencia = true;

        equipos.forEach(function (equipo) {
            if (info.equipo === equipo.equipo) {
                existencia = false;
            }
        });

        if (existencia) {
            info.conexiones = [];
            info.conexiones.push(socket.id);
            equipos.push(info);
            publicSocket.emit("equipoIngresado");
            publicSocket.emit("actualizarEquipos", {"equipos": equipos});
        } else {
            publicSocket.emit("equipoExiste");
        }

    });
    socket.on('registrarEquipo', function (info) {
        //verificar cuando ingresa por primera vez un cliente 

        let existencia = true;

        equipos.forEach(function (equipo) {
            if (info.equipo === equipo.equipo) {
                existencia = false;
                equipo.conexiones.push(socket.id);
            }
        });

        if (existencia) { //no existe en la lista
            info.conexiones = [];
            info.conexiones.push(socket.id);

            equipos.push(info);
        }

        publicSocket.emit("actualizarEquipos", {"equipos": equipos});

    });

    socket.on('cerrarSesion', function (sesion) {
        // elmina un equipo en la lista

        let contador = 0;
        // console.log(equipos);
        equipos.forEach(function (equipo) {
            if (sesion.equipo === equipo.equipo) {
                console.log(equipo.equipo);
                console.log("cont" + contador);
                equipos.splice(contador, 1);
            }

            contador++;
        });
        // console.log(equipos);
        publicSocket.emit("sesionEliminada");
        publicSocket.emit("actualizarEquipos", {"equipos": equipos});

    });

    socket.on('actualizarEquipo', function (info) {
        //actualizar un equipo 

        let cuenta = 0;
        equipos.forEach(function (equipo) {
            if (info.equipo === equipo.equipo) {
                info.conexiones = equipos[cuenta].conexiones;
                equipos[cuenta] = info;
                publicSocket.emit('equipoActualizado');
                publicSocket.emit("actualizarEquipos", {"equipos": equipos});
            }
            cuenta++;
        });


    });


    socket.on('disconnect', function (tab) {
        publicSocket.emit('getAllSockets', {cantidad: getAllComputer(), ayuda: personasAyuda, equipos: equipos});

        //borrarlo
        contadorConexiones = 0;
        equipos.forEach(function (equipo) {
            if (equipo.conexiones.length > 0) {
                equipo.conexiones.forEach(function (conexion) {

                    if (conexion == socket.id) {
                        equipo.conexiones.splice(contadorConexiones, 1);
                        publicSocket.emit("actualizarEquipos", {"equipos": equipos});
                    }
                    contadorConexiones++;
                });
                contadorConexiones = 0;
            }
        });

    });


}
