exports.crearPDF = function (asistente, callback) {
    // console.log(asistente);
    if (asistente) {
        var pdf = require('pdfkit');
        var fs = require('fs');
        var longNombre = 12;


        //if (asistente.name.length >= 25) {
          //  longNombre = 18;
        //}


        var doc = new pdf({
            margin: 0,
        });

        writeStream = fs.createWriteStream('./views/public/pdf/' + asistente.cc + '.pdf');
        doc.pipe(writeStream);

        doc.registerFont('BentonBold', 'views/public/fonts/BentonSansBBVA-Bold.otf')
        doc.registerFont('BentonLight', 'views/public/fonts/BentonSansBBVA-Light.otf')
        doc.registerFont('BentonMedium', 'views/public/fonts/BentonSansBBVA-Medium.otf')
        doc.registerFont('BentonMediumItalic', 'views/public/fonts/BentonSansBBVA-MediumItalic.otf')

        //Titulos

        doc.font('BentonBold');


        //Info
        //doc.font('Sebino');


        doc.moveDown(11.6);
        var ancho = 618;
        //612 width

        while (doc.widthOfString(asistente.name) >= 230) {
            longNombre=longNombre-0.1;
            doc.fontSize(longNombre);
        }

        doc.fontSize(longNombre)
            .text(asistente.name, {
                align: 'center',
                width: ancho,
            }).moveDown(0.01);

        doc.font('BentonMedium');

        doc.fontSize(10)
            .text(asistente.territorio, {
                align: 'center',
                width: ancho,
            }).moveDown(0.01);




        doc.fontSize(10)
            .text("CC: " + asistente.cc, {
                width: ancho,
                align: 'center'
            }).moveDown(0.01);

        doc.font('BentonLight');

        doc.fontSize(10)
            .text(asistente.hotel + " - " + asistente.habitacion, {
                width: ancho,
                align: 'center'
            });

        //doc.rect(163.5, 67, 288, 185).stroke();
        doc.end();

        writeStream.on('finish', function () {
            callback(asistente.cc);
            writeStream.end();
        });
    }


}
