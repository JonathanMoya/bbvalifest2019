module.exports = function (bd, file) {
    fs.createReadStream('public/excel/listadoMiercoles.csv')
        .pipe(csv({separator: ';', headers: ['Nombre', 'CC', 'Cargo']}))
        .on('data', (data) => results.push(data))
        .on('end', () => {
            let array = [];
            console.log(hasDupes(results));
            res.send(results);
            results.forEach(function (element) {
                let name = element.Nombre;
                let cc = element.CC;
                let cargo = element.Cargo;
                if (Number(cc).toString() === 'NaN') {
                    console.log('cedulaIncorrecta');
                    console.log(cc);
                } else {
                    let json = {
                        name: name,
                        cc: cc,
                        cargo: cargo,
                        asistencia: false,
                        fecha: 0
                    };
                    array.push(json);
                }
            });
            bd.createDB(array, function (callback) {
            })
        });
}
