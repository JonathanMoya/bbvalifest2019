const Excel = require('exceljs/modern.nodejs');
const bd = require('./bd.js');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csv = require('csv-parser');

module.exports = function (socket, equipos) {

    socket.on('getBU', function () {
        const testFolder = './BU/';
        const fs = require('fs');
        fs.readdir(testFolder, (err, files) => {
            socket.emit('getBU', files);
        });
    });

    socket.on('uploadBU', function (name) {
        var fs = require('fs');
        let results = [];
        fs.createReadStream('BU/' + name)
            .pipe(csv({headers: ['CC', 'Nombre', 'Territorio', 'Hotel', 'Habitacion', 'Asistencia', 'Fecha']}))
            .on('data', (data) => {
                results.push(data);
            })
            .on('end', () => {
                let array = [];
                results.forEach(function (element) {
                    let name = element.Nombre;
                    let cc = element.CC;
                    let hotel = element.Hotel;
                    let habitacion = element.Habitacion;
                    let territorio = element.Territorio;
                    let asistencia = element.Asistencia;
                    let fecha = element.fecha;
                    if (Number(cc).toString() === 'NaN') {
                        console.log('cedulaIncorrecta');
                        console.log(cc);
                    } else {
                        let json;
                        if (asistencia === 'true') {
                            json = {
                                name: name,
                                cc: cc,
                                territorio: territorio,
                                habitacion: habitacion,
                                hotel: hotel,
                                asistencia: asistencia,
                                fecha: fecha,
                            };
                        } else {
                            json = {
                                name: name,
                                cc: cc,
                                territorio: territorio,
                                habitacion: habitacion,
                                hotel: hotel,
                                asistencia: false,
                            };
                        }
                        array.push(json);
                    }
                });
                bd.createDB(array, function (callback) {
                })
            });
    })


    // socket.on('uploadBU', function (name) {
    //     var fs = require('fs');
    //     let results = [];
    //     fs.createReadStream('BU/' + name)
    //         .pipe(csv({headers: ['Nombre', 'CC', 'Hotel', 'Habitacion', 'Asistencia', 'Fecha']}))
    //         .on('data', (data) => {
    //             results.push(data);
    //         })
    //         .on('end', () => {
    //             let array = [];
    //             results.forEach(function (element) {
    //                 console.log(element);
    //                 let name = element.Nombre;
    //                 let cc = element.CC;
    //                 // console.log(cc + ' ' + element.CC);
    //                 let hotel = element.Hotel;
    //                 let habitacion = element.Habitacion;
    //                 let asistencia = element.Asistencia;
    //                 let fecha = element.fecha;
    //                 // console.log(cc);
    //                 if (Number(cc).toString() === 'NaN') {
    //                     console.log('cedulaIncorrecta');
    //                     // console.log(cc);
    //                 } else {
    //                     let json;
    //                     if (asistencia === 'true') {
    //                         json = {
    //                             name: name,
    //                             cc: cc,
    //                             hotel: hotel,
    //                             habitacion: habitacion,
    //                             asistencia: asistencia,
    //                             fecha: fecha,
    //                         };
    //                     } else {
    //                         json = {
    //                             name: name,
    //                             cc: cc,
    //                             hotel: hotel,
    //                             habitacion: habitacion,
    //                             asistencia: false,
    //                         };
    //                     }
    //                     array.push(json);
    //                 }
    //             });
    //             bd.createDB(array, function (callback) {
    //             })
    //         });
    // })

    socket.on('generateReport', function () {
        var workbook = new Excel.Workbook();
        var sheet = workbook.addWorksheet('Asistencias');
        var sheet2 = workbook.addWorksheet('Pendientes');
        var sheet3 = workbook.addWorksheet('Todos');
        sheet.columns = [
            {header: 'CC', key: 'cc', width: 20},
            {header: 'Nombre', key: 'name', width: 40},
            {header: 'Hotel', key: 'hotel', width: 40},
            {header: 'Habitacion', key: 'habitacion', width: 40},
            {header: 'Hora de llegada', key: 'fecha', width: 30}
        ];
        sheet2.columns = [
            {header: 'CC', key: 'cc', width: 20},
            {header: 'Nombre', key: 'name', width: 40},
            {header: 'Hotel', key: 'hotel', width: 40},
            {header: 'Habitacion', key: 'habitacion', width: 40},
        ];
        sheet3.columns = [
            {header: 'CC', key: 'cc', width: 20},
            {header: 'Nombre', key: 'name', width: 40},
            {header: 'Hotel', key: 'hotel', width: 40},
            {header: 'Habitacion', key: 'habitacion', width: 40},
        ];
        bd.getDB(function (callback) {
            callback.forEach(function (el) {
                if (el.asistencia) {
                    let hora = el.fecha.getHours().toString();
                    let minutos = el.fecha.getMinutes().toString();
                    if (el.fecha.getHours() <= 9) {
                        hora = '0' + el.fecha.getHours().toString();
                    }
                    if (el.fecha.getMinutes() <= 9) {
                        minutos = '0' + el.fecha.getMinutes().toString();
                    }
                    dateFinal = hora + ':' + minutos;
                    var jsonData = {
                        cc: el.cc,
                        name: el.name,
                        hotel: el.hotel,
                        habitacion: el.habitacion,
                        fecha: dateFinal
                    };
                    sheet.addRow(jsonData);
                } else {
                    var jsonData = {
                        cc: el.cc,
                        name: el.name,
                        hotel: el.hotel,
                        habitacion: el.habitacion,
                    };
                    sheet2.addRow(jsonData);
                }
                var jsonDataFull = {
                    cc: el.cc,
                    name: el.name,
                    hotel: el.hotel,
                    habitacion: el.habitacion,
                };
                sheet3.addRow(jsonDataFull);
            });
            workbook.xlsx.writeFile("AdminData/informe.xlsx").then(function () {
                console.log("xls file is written.");
            });
        })
    });

    socket.on('generateBU', function () {
        var date = new Date();
        var dataUp = [];
        bd.getDB(function (callback) {
            callback.forEach(function (el) {
                let asistencia;
                if (el.asistencia) {
                    asistencia = 'true'
                } else {
                    asistencia = 'false'
                }
                let fecha;
                if (el.fecha !== 0 && el.fecha !== undefined && el.fecha !== null) {
                    fecha = el.fecha.toJSON();
                } else {
                    fecha = 0
                }
                console.log(el.territorio);
                let json = {
                    cc: el.cc.replace(/\s/g, ''),
                    name: el.name,
                    territorio: el.territorio,
                    hotel: el.hotel,
                    habitacion: el.habitacion,
                    fecha: fecha,
                    asistencia: asistencia
                };
                dataUp.push(json);
            });
            let hora = date.getHours().toString();
            let minutos = date.getMinutes().toString();
            if (date.getHours() <= 9) {
                hora = '0' + date.getHours().toString();
            }
            if (date.getMinutes() <= 9) {
                minutos = '0' + date.getMinutes().toString();
            }
            let day = date.getDate();
            console.log(day);
            let nameString = day + " | " + hora + '-' + minutos + ".csv";

            const csvWriter = createCsvWriter({
                path: 'BU/' + nameString,
                header: ['cc', 'name', 'territorio', 'hotel', 'habitacion', 'asistencia', 'fecha']
            });

            csvWriter
                .writeRecords(dataUp)
                .then(() => console.log('BU Creado' + nameString));

        })
    });
}
