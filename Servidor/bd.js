var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/shockDB";

MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
    if (err) throw err;
    var dbo = db.db("BBVAFest2019");
    console.log('Base de datos conectada');
    dbo.createCollection("asistentes", function (err, res) {
        if (err) throw err;
        db.close();
    });
});

exports.createDB = function (data, callback) {
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
        var dbo = db.db("BBVAFest2019");
        dbo.collection("asistentes").drop(function (err, delOK) {
            if (err) throw err;
            // if (delOK) console.log("Collection deleted");
            db.close();
        });
        dbo.collection("asistentes").insertMany(data, function (err, res) {
            if (err) throw err;
            callback(res.insertedCount);
            db.close();
        });
    });
};

exports.getDB = function (callback) {
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
        if (err) throw err;
        var dbo = db.db("BBVAFest2019");
        dbo.collection("asistentes").find({}).toArray(function (err, result) {
            if (err) throw err;
            callback(result);
            db.close();
        });
    });
};

exports.registrarPersona = function (cedula, callback) {
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
        if (err) throw err;
        var dbo = db.db("BBVAFest2019");
        var myquery = {cc: cedula};
        var date = new Date();
        var newvalues = {$set: {asistencia: true, fecha: date}};
        dbo.collection("asistentes").updateOne(myquery, newvalues, function (err, res) {
            if (err) throw err;
            callback("1 document updated");
            db.close();
        });
    });
};

exports.undoRegistrar = function (cedula, callback) {
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
        if (err) throw err;
        var dbo = db.db("BBVAFest2019");
        var myquery = {cc: cedula};
        var newvalues = {$set: {asistencia: false, fecha: 0}};
        dbo.collection("asistentes").updateOne(myquery, newvalues, function (err, res) {
            if (err) throw err;
            callback("1 document updated");
            db.close();
        });
    });
};

exports.buscarPersonaCC = function (cc, callback) {
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
        if (err) throw err;
        var dbo = db.db("BBVAFest2019");
        dbo.collection("asistentes").find({cc: cc}).toArray(function (err, result) {
            if (err) throw err;
            callback(result);
            db.close();
        });
    });
}

exports.newPerson = function (data, callback) {
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
        if (err) throw err;
        var dbo = db.db("BBVAFest2019");
        dbo.collection("asistentes").insertOne(data, function (err, res) {
            if (err) throw err;
            callback(res.insertedCount);
            db.close();
        });
    });
};

exports.deletePerson = function (cc, callback) {
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, db) {
        if (err) throw err;
        var dbo = db.db("BBVAFest2019");
        dbo.collection("asistentes").deleteOne({cc: cc}, function (err, obj) {
            if (err) throw err;
            callback("1 document deleted");
            db.close();
        });
    });
};
