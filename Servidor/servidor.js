const express = require('express');
const servidor = express();
const server = require('http').createServer(servidor);
var cors = require('cors')
const fs = require('fs');
const csv = require('csv-parser');
const bd = require('./bd.js');
const pdf = require('./pdf.js');
var io = require('socket.io')(server);
const Excel = require('exceljs/modern.nodejs');
var control = require('./control.json');
let equipos = [];

const publicSocket = io.of('/public');
const adminSocket = io.of('/admin');

servidor.use(cors())
servidor.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, Accept, Accept-Language, Content-Language, DPR, Downlink, Save-Data X-API-KEY, Origin, X-Frame-Options, X-Requested-With, Frame-Options, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
}); 

/* servidor.use(helmet.frameguard({
    frameguard: {
        action: "allow-from",
        domain: "*"
      }
  })); */
  
//servidor.use('/', express.static(__dirname + '/views/public'));
servidor.use('/', express.static(__dirname + '/views/public', {
    setHeaders: function(res, path) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header('Access-Control-Allow-Headers', 'Authorization, Accept, Accept-Language, Content-Language, DPR, Downlink, Save-Data X-API-KEY, Origin, X-Frame-Options, X-Requested-With, Frame-Options, Content-Type, Accept, Access-Control-Allow-Request-Method');
      res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
      res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    }
  }));

var bodyParser = require('body-parser');
servidor.use(bodyParser.json());       // to support JSON-encoded bodies
servidor.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

servidor.get('/', function (req, res) {
    let results = [];

    fs.createReadStream('public/excel/database.csv')
        .pipe(csv({separator: ';', headers: ['CC', 'Nombre', 'Territorio', 'Hotel', 'Habitacion']}))
        .on('data', (data) => results.push(data))
        .on('end', () => {
            let array = [];
            console.log(hasDupes(results));
            res.send(results);
            count = 0;
            results.forEach(function (element) {
                count++;
                let cc = element.CC;
                let name = element.Nombre;
                let territorio = element.Territorio;
                let hotel = element.Hotel;
                let habitacion = element.Habitacion;
                if (Number(cc).toString() === 'NaN') {
                    console.log('cedulaIncorrecta');
                    console.log(cc);
                    console.log(count);
                } else {
                    let json = {
                        cc: cc,
                        name: name,
                        territorio: territorio,
                        hotel: hotel,
                        habitacion: habitacion,
                        asistencia: false,
                        fecha: 0
                    };
                    array.push(json);
                }
            });
            bd.createDB(array, function (callback) {
            })
        });
});

servidor.get('/bd', function (req, res) {
});


function hasDupes(array) {
    const seen = new Set();
    const arr = [
        {id: 1, name: "test1"},
        {id: 2, name: "test2"},
        {id: 2, name: "test3"},
        {id: 2, name: "test3"},
        {id: 2, name: "test3"},
        {id: 3, name: "test4"},
        {id: 4, name: "test5"},
        {id: 5, name: "test6"},
        {id: 5, name: "test7"},
        {id: 6, name: "test8"}
    ];
    let boolean = false;
    const filteredArr = array.filter(el => {
        const duplicate = seen.has(el.CC);
        seen.add(el.CC);
        if (duplicate) {
            console.log(el.CC);
            boolean = true;
        }
        // return !duplicate;
    });
    return boolean;
}


adminSocket.on('connection', function (socket) {
    require('./socketAdmin')(socket, equipos);
});

function intervalFunc() {
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    var date = new Date();
    var dataUp = [];
    bd.getDB(function (callback) {
        callback.forEach(function (el) {
            let asistencia;
            if (el.asistencia) {
                asistencia = 'true'
            } else {
                asistencia = 'false'
            }
            let fecha;
            if (el.fecha !== 0 && el.fecha !== undefined && el.fecha !== null) {
                fecha = el.fecha.toJSON();
            } else {
                fecha = 0
            }
            let json = {
                cc: el.cc,
                name: el.name,
                hotel: el.hotel,
                hotel: el.habitacion,
                fecha: fecha,
                asistencia: asistencia
            };
            dataUp.push(json);
        });
        let hora = date.getHours().toString();
        let minutos = date.getMinutes().toString();
        if (date.getHours() <= 9) {
            hora = '0' + date.getHours().toString();
        }
        if (date.getMinutes() <= 9) {
            minutos = '0' + date.getMinutes().toString();
        }
        let nameString = hora + '-' + minutos + ".csv";

        const csvWriter = createCsvWriter({
            path: 'BU/' + nameString,
            header: ['name', 'cc', 'hotel', 'habitacion', 'asistencia', 'fecha']
        });

        csvWriter
            .writeRecords(dataUp)
            .then(() => console.log('BU Creado ' + nameString));

    })
}

setInterval(intervalFunc, 600000);


publicSocket.on('connection', function (socket) {
    require('./socketPublic.js')(socket, publicSocket, io, bd, equipos, pdf)
});

let puerto = 8080;
server.listen(puerto, function (err) {
    console.log("Control para " + control.nombre + " corriendo en " + puerto);
});
