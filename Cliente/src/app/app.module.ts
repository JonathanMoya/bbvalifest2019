import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {HomeComponent} from './Components/home/home.component';
import {RouterModule, Routes} from '@angular/router';
import {NavbarComponent} from './Components/navbar/navbar.component';
import {SocketIoModule} from 'ngx-socket-io';
import {SocketPublicService} from './services/socket-public.service';
import {SocketAdminService} from './services/socket-admin.service';
import {FormsModule} from '@angular/forms';
import {AdminComponent} from './Components/admin/admin.component';
import {AddComponent} from './Components/add/add.component';
import {FooterComponent} from './Components/footer/footer.component';
import {ConsultaComponent} from './Components/consulta/consulta.component';
import {HttpClientModule} from '@angular/common/http';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { FileSaverModule } from 'ngx-filesaver';


const appRoutes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AdminComponent,
    AddComponent,
    FooterComponent,
    ConsultaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StorageServiceModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false,
      }
    ),
    SocketIoModule,
    FormsModule,
    HttpClientModule,
    FileSaverModule 
  ],
  providers: [SocketPublicService, SocketAdminService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
