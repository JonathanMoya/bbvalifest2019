import {AfterViewInit, Component, OnInit} from '@angular/core';
import {SocketPublicService} from '../../services/socket-public.service';
import {Observable} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';
import {HttpClient} from '@angular/common/http';
import {FileSaverService} from 'ngx-filesaver';

declare var imprimir: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  personas: any = '';
  personas$: Observable<any[]>;
  imprimirPDF$: Observable<any[]>;
  imprimirPDFAliado$: Observable<any>;
  equiposRes$: Observable<any>;
  viewPerson = 'todos';
  search = '';
  allData: any[] = [];
  dateRevisar = '';
  equipos = [];
  impresoraSelect = '0';
  esteEquipo = '';

  constructor(private socketPublic: SocketPublicService, @Inject(LOCAL_STORAGE) private storage: StorageService, private _http: HttpClient, private _FileSaverService: FileSaverService) {
    if (this.storage.get('equipo')) {
      if (this.storage.get('equipo').impresoraSelect) {
        this.impresoraSelect = this.storage.get('equipo').impresoraSelect;
      }
    }
  }

  cambiaSelect() {
    console.log('cambio');
    if (this.storage.get('equipo')) {
      let objeto = this.storage.get('equipo');
      objeto.impresoraSelect = this.impresoraSelect;
      this.storage.set('equipo', objeto);
      console.log(this.storage.get('equipo').impresoraSeleccion);
    }
  }


  ngOnInit() {
    this.personas$ = this.socketPublic.fromEvent('personasAll');
    this.personas$.subscribe(data => {
      this.changeData(data);
      console.log(data);
      this.allData = data;
      if (this.storage.get('equipo')) {
        this.socketPublic.emit('registrarEquipo', this.storage.get('equipo'));
      }
    });

    this.imprimirPDF$ = this.socketPublic.fromEvent('imprimirPDF');
    this.imprimirPDF$.subscribe(persona => {
      console.log('imprimirPDF');
      imprimir(persona);

    });

    this.imprimirPDFAliado$ = this.socketPublic.fromEvent('imprimirPDFAliado');
    this.imprimirPDFAliado$.subscribe(datos => {
      console.log('imprimirPDFAliado');
      if (this.storage.get('equipo')) {
        if (datos.impresora === this.storage.get('equipo').equipo) {
          console.log(datos);
          imprimir(datos.persona);
        }
      }
    });

    this.equiposRes$ = this.socketPublic.fromEvent('actualizarEquipos');
    this.equiposRes$.subscribe(data => {
      this.equipos = data.equipos;
      if (this.storage.get('equipo')) {
        this.esteEquipo = this.storage.get('equipo').equipo;
      }

      console.log(this.equipos);
    });

    this.socketPublic.fromEvent('connect').subscribe(data => {
      const css = document.getElementById('modalDisconect').style.cssText;
      if (css === 'display: block;') {
        document.getElementById('disconnectSocket').click();
      }

      console.log(css);
    });
    this.socketPublic.on('disconnect', () => {
      document.getElementById('disconnectSocket').click();
    });

  }

  registrarPersona(persona) {
    setTimeout(() => {
        this.search = '';
        this.changeData(this.allData);
      },
      1500);
    this.socketPublic.emit('registrarPersona', {persona: persona});
    this.socketPublic.emit('imprimirPDF', {persona: persona, impresora: this.impresoraSelect});
  }

  imprimirPersona(persona) {
    console.log(this.impresoraSelect);
    this.socketPublic.emit('imprimirPDF', {persona: persona, impresora: this.impresoraSelect});
  }


  changeData(data: any[]) {
    setTimeout(() => {
        const view = this.viewPerson;
        const busqueda = this.search;
        const array = [];
        let Bool = true;
        let i = 0;
        if (Number(busqueda).toString() !== 'NaN') {
          while (Bool) {
            const name = data[i].cc;
            if (name.startsWith(busqueda)) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
              console.log('termine');
            }
          }
        } else {
          while (Bool) {
            const name = data[i].name.toUpperCase().replace(/\s/g, '');
            if (name.indexOf(busqueda.toLocaleUpperCase().replace(/\s/g, '')) > -1) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
              console.log('termine');
            }
          }
        }
        this.personas = array;
      },
      10);
  }

  viewDate(cedula: string) {
    let dateFinal = '';
    // tslint:disable-next-line:only-arrow-functions
    this.allData.forEach(function(el) {
      if (el.cc === cedula) {
        console.log(el);
        console.log(el.fecha);
        const date = new Date(el.fecha);
        let hora = date.getHours().toString();
        let minutos = date.getMinutes().toString();
        if (date.getHours() <= 9) {
          hora = '0' + date.getHours().toString();
        }
        if (date.getMinutes() <= 9) {
          minutos = '0' + date.getMinutes().toString();
        }
        dateFinal = date.toLocaleDateString() + ' ' + hora + ':' + minutos;
      }
    });
    this.dateRevisar = dateFinal;
  }

  pedirAyuda() {
    console.log('aiuda');
    this.socketPublic.emit('pedirAyuda');
    alert('Se envio tu petición');
  }

}
