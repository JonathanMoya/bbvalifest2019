import {Component, OnInit} from '@angular/core';
import {SocketPublicService} from '../../services/socket-public.service';
import {SocketAdminService} from '../../services/socket-admin.service';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';

interface ModalEditar {
  name: string;
  cc: string;
  hotel: string;
  habitacion: string;
  territorio: string;
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  cSockets$: Observable<any>;
  getBU$: Observable<any>;
  editModal: ModalEditar = {
    cc: '',
    name: '',
    hotel: '',
    habitacion: '',
    territorio: ''
  };
  equiposRes$: Observable<any>;
  getBU: any = [];
  editCedula = '';
  cSockets: any = ['', ''];
  ayuda: any = ['', ''];
  personas: any = '';
  personas$: Observable<any[]>;
  response$: Observable<any>;
  viewPerson = 'todos';
  search = '';
  allData: any[] = [];
  dateRevisar = '';

  newName = '';
  newCC = '';
  newTerritorio = '';
  newHotel = '';
  newHabitacion = '';

  alertState = 'alert-dark';
  textAlert = 'Estado desconocido';
  equipos = [];

  constructor(private socketPublic: SocketPublicService, private socketAdmin: SocketAdminService,
              private http: HttpClient, @Inject(LOCAL_STORAGE) private storage: StorageService) {
  }

  ngOnInit() {
    this.socketPublic.emit('getAllSockets');
    this.cSockets$ = this.socketPublic.fromEvent('getAllSockets');
    this.getBU$ = this.socketAdmin.fromEvent('getBU');
    this.personas$ = this.socketPublic.fromEvent('personasAll');
    this.response$ = this.socketPublic.fromEvent('responseNewPerson');
    this.equiposRes$ = this.socketPublic.fromEvent('actualizarEquipos');

    this.cSockets$.subscribe(data => {
      console.log(data);
      this.cSockets = data.cantidad;
      this.ayuda = data.ayuda;
      this.equipos = data.equipos;
      console.log(this.equipos);
    });
    this.getBU$.subscribe(data => {
      console.log(data);
      console.log('BU');
      this.getBU = data;
    });
    this.personas$.subscribe(data => {
      this.changeData(data);
      this.allData = data;
      console.log(data);
      if (this.storage.get('equipo')) {
        this.socketPublic.emit('registrarEquipo', this.storage.get('equipo'));
      }
    });
    this.response$.subscribe(data => {
      console.log(data);
      if (data.res === 1) {
        this.newCC = '';
        this.newTerritorio = '';
        this.newHabitacion = '';
        this.newHotel = '';
        this.newName = '';
        alert('Persona Agregada ');
      } else {
        alert('Cédula duplicada');
        this.newCC = '';
      }
    });

    this.equiposRes$.subscribe(data => {
      this.equipos = data.equipos;
      console.log(this.equipos);
    });

    this.socketPublic.fromEvent('connect').subscribe(data => {
      this.alertState = 'alert-success';
      this.textAlert = 'Servidor corriendo';
    });
    this.socketPublic.fromEvent('disconnect').subscribe(data => {
      this.alertState = 'alert-danger';
      this.textAlert = 'Servidor desconectado';
    });
  }

  // Obtener IP
  getNameIp(ip: string): string {
    let name = '';
    switch (ip) {
      case '192.168.0.8':
        name = 'Tab 1';
        break;
      case '192.168.0.7':
        name = 'Tab 2';
        break;
      case '192.168.0.6':
        name = 'Tab 3';
        break;
      case '192.168.0.4':
        name = 'Tab 4';
        break;
      case '192.168.0.2':
        name = 'Macbook Pro';
        break;
    }
    return name;
  }

  // Terminar ayuda
  ayudaTerminar(socket) {
    console.log(socket);
    this.socketPublic.emit('terminarAyuda', socket);
  }

  // Generar informe
  ReportG() {
    this.socketAdmin.emit('generateReport');
  }

  // Generar Back Up
  BackUpG() {
    this.socketAdmin.emit('generateBU');
  }

  // Obtener base de datos
  getBUServer() {
    this.socketAdmin.emit('getBU');
  }

  // Registrar persona
  registrarPersona(cedula: string) {
    this.socketPublic.emit('registrarPersona', cedula);
  }

  // Subir Back Up
  uploadBU(name) {
    this.socketAdmin.emit('uploadBU', name);
  }

  // Busqueda
  changeData(data: any[]) {
    setTimeout(() => {
        const view = this.viewPerson;
        const busqueda = this.search;
        const array = [];
        let Bool = true;
        let i = 0;
        if (Number(busqueda).toString() !== 'NaN') {
          while (Bool) {
            const name = data[i].cc;
            if (name.startsWith(busqueda)) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
              console.log('termine');
            }
          }
        } else {
          while (Bool) {
            const name = data[i].name.toUpperCase().replace(/\s/g, '');
            if (name.indexOf(busqueda.toLocaleUpperCase().replace(/\s/g, '')) > -1) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
              console.log('termine');
            }
          }
        }
        this.personas = array;
      },
      10);
  }

  // Borrar persona
  borrar(cedula: string) {
    this.socketPublic.emit('deletePerson', cedula);
  }

  // Undo registrar
  undoRegistrar(cedula: string) {
    this.socketPublic.emit('undoRegistrar', cedula);
  }

  // Ver fecha registro
  viewDate(cedula: string) {
    let dateFinal = '';
    this.allData.forEach((el) => {
      if (el.cc === cedula) {
        const date = new Date(el.fecha);
        let hora = date.getHours().toString();
        let minutos = date.getMinutes().toString();
        if (date.getHours() <= 9) {
          hora = '0' + date.getHours().toString();
        }
        if (date.getMinutes() <= 9) {
          minutos = '0' + date.getMinutes().toString();
        }
        // dateFinal = date.toLocaleDateString() + ' ' + hora + ':' + minutos;

        dateFinal = el.fecha.toString();
      }
    });
    this.dateRevisar = dateFinal;
  }

  // Format nombre
  upperCase() {
    setTimeout(() => {
        const name = this.newName.split('');
        if (name.length === 1) {
          this.newName = this.newName.toUpperCase();
        } else {
          if (name[name.length - 2] === ' ') {
            name[name.length - 1] = name[name.length - 1].toUpperCase();
            let newName = '';
            name.forEach((el) => {
              newName += el;
            });
            this.newName = newName;
          }
        }
      },
      10);
  }

  // Registrar nueva persona
  sendNewValues() {
    if (this.newName === '' || this.newTerritorio === '' || this.newCC === '' || this.newHotel === '' || this.newHabitacion === '') {
      alert('Completa todos los campos');
    } else {
      const json = {
        name: this.newName,
        cc: this.newCC.toString(),
        terrotorio: this.newTerritorio,
        hotel: this.newHotel,
        habitacion: this.newHabitacion
      };
      this.socketPublic.emit('newPerson', json);
    }
  }

  // upload() {
  //   this.http.post('http://localhost:8080/uploadBU', 'asd')
  //     .subscribe((response) => {
  //       console.log('response received is ', response);
  //     });
  // }
  //
  // postMethod(files: FileList) {
  //   const fileToUpload = files.item(0);
  //   const formData = new FormData();
  //   formData.append('file', fileToUpload, fileToUpload.name);
  //   console.log(formData);
  //   this.http.post('http://localhost:8080/uploadBU', formData).subscribe((val) => {
  //     console.log(val);
  //   });
  //   return false;
  // }

  // Abrir modal con los datos
  Editar(cc) {
    // document.getElementById('openModalEdit').click();
    // this.editModal.cc =
    console.log(this.allData.length);
    let encontrado = true;
    let i = 0;
    while (encontrado) {
      if (cc === this.allData[i].cc) {
        console.log(this.allData[i]);
        this.editModal.cc = this.allData[i].cc;
        this.editCedula = this.allData[i].cc;
        this.editModal.name = this.allData[i].name;
        this.editModal.hotel = this.allData[i].hotel;
        this.editModal.habitacion = this.allData[i].habitacion;
        this.editModal.territorio = this.allData[i].territorio;
        encontrado = false;
      } else {
        i++;
      }
      if (i === this.allData.length) {
        encontrado = false;
        alert('No se encontro la persona');
        console.log('termine');
      }
    }
    // }
    //   if (this.newName === '' || this.newCargo === '' || this.newCC === '') {
    //     alert('Completa todos los campos');
    //   } else {
    //     const json = {
    //       name: this.newName,
    //       cc: this.newCC.toString(),
    //       cargo: this.newCargo
    //     };
    //     this.socketPublic.emit('newPerson', json);
    //   }
  }

  // Enviar datos editados
  sendEditar() {
    console.log('Hola');
    console.log(this.editModal);
    this.socketPublic.emit('updateData', {cedula: this.editCedula, data: this.editModal});
    this.editCedula = '';
  }

}
