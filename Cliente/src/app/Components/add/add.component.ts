import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {SocketPublicService} from '../../services/socket-public.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  personas: any = '';
  personas$: Observable<any[]>;
  response$: Observable<any>;
  viewPerson = 'todos';
  search = '';
  allData: any[] = [];
  dateRevisar = '';
  newName = '';
  newCC = '';
  newCargo = '';

  constructor(private socketPublic: SocketPublicService) {
  }

  ngOnInit() {
    this.personas$ = this.socketPublic.fromEvent('personasAll');
    this.response$ = this.socketPublic.fromEvent('responseNewPerson');
    this.personas$.subscribe(data => {
      this.changeData(data);
      this.allData = data;
      console.log(data);
    });
    this.response$.subscribe(data => {
      console.log(data);
      if (data.res === 1) {
        this.newCC = '';
        this.newCargo = '';
        this.newName = '';
      } else {
        alert('Cédula duplicada');
        this.newCC = '';
      }
    });

    // tslint:disable-next-line:only-arrow-functions
    this.socketPublic.on('disconnect', function() {
      document.getElementById('disconnectSocket').click();
      // this.modalConection = 'block';
    });
    // tslint:disable-next-line:only-arrow-functions
    this.socketPublic.on('connect', function() {
      const css = document.getElementById('modalDisconect').style.cssText;
      if (css === 'display: block;') {
        document.getElementById('disconnectSocket').click();
      }
      console.log(css);
      // alert('conect');
    });
  }

  registrarPersona(cedula: string) {
    setTimeout(() => {
        this.search = '';
        this.changeData(this.allData);
      },
      2500);
    this.socketPublic.emit('registrarPersona', cedula);
  }

  changeData(data: any[]) {
    setTimeout(() => {
        const view = this.viewPerson;
        const busqueda = this.search;
        const array = [];
        let Bool = true;
        let i = 0;
        if (Number(busqueda).toString() !== 'NaN') {
          while (Bool) {
            const name = data[i].cc;
            if (name.startsWith(busqueda)) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
            }
          }
        } else {
          while (Bool) {
            const name = data[i].name.toUpperCase();
            if (name.startsWith(busqueda.toLocaleUpperCase())) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
            }
          }
        }
        this.personas = array;
      },
      10);
  }

  viewDate(cedula: string) {
    let dateFinal = '';
    this.allData.forEach(function(el) {
      if (el.cc === cedula) {
        const date = new Date(el.fecha);
        let hora = date.getHours().toString();
        let minutos = date.getMinutes().toString();
        if (date.getHours() <= 9) {
          hora = '0' + date.getHours().toString();
        }
        if (date.getMinutes() <= 9) {
          minutos = '0' + date.getMinutes().toString();
        }
        dateFinal = date.toLocaleDateString() + ' ' + hora + ':' + minutos;
      }
    });
    this.dateRevisar = dateFinal;
  }

  upperCase() {
    setTimeout(() => {
        const name = this.newName.split('');
        if (name.length === 1) {
          this.newName = this.newName.toUpperCase();
        } else {
          if (name[name.length - 2] === ' ') {
            name[name.length - 1] = name[name.length - 1].toUpperCase();
            let newName = '';
            name.forEach(function(el) {
              newName += el;
            });
            this.newName = newName;
          }
        }
      },
      10);
  }

  sendNewValues() {
    if (this.newName === '' || this.newCargo === '' || this.newCC === '') {
      alert('Completa todos los campos');
    } else {
      const json = {
        name: this.newName,
        cc: this.newCC.toString(),
        cargo: this.newCargo
      };
      this.socketPublic.emit('newPerson', json);
    }
  }

  pedirAyuda() {
    this.socketPublic.emit('pedirAyuda');
    alert('Se envio tu petición');
  }

}
