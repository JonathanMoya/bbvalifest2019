import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {SocketPublicService} from '../../services/socket-public.service';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {
  personas: any = '';
  personas$: Observable<any[]>;
  viewPerson = 'todos';
  search = '';
  allData: any[] = [];
  dateRevisar = '';

  constructor(private socketPublic: SocketPublicService) {
  }

  ngOnInit() {
    this.personas$ = this.socketPublic.fromEvent('personasAll');
    this.personas$.subscribe(data => {
      this.changeData(data);
      this.allData = data;
    });

    this.socketPublic.fromEvent('connect').subscribe(data => {
      const css = document.getElementById('modalDisconect').style.cssText;
      if (css === 'display: block;') {
        document.getElementById('disconnectSocket').click();
      }

      console.log(css);
    });
    // tslint:disable-next-line:only-arrow-functions
    this.socketPublic.on('disconnect', function() {
      document.getElementById('disconnectSocket').click();
      // this.modalConection = 'block';
    });
    // tslint:disable-next-line:only-arrow-functions
    // this.socketPublic.on('connect', function() {
    //   const css = document.getElementById('modalDisconect').style.cssText;
    //   if (css === 'display: block;') {
    //     document.getElementById('disconnectSocket').click();
    //   }
    //   console.log(css);
    //   // alert('conect');
    // });
  }

  changeData(data: any[]) {
    setTimeout(() => {
        const view = this.viewPerson;
        const busqueda = this.search;
        const array = [];
        let Bool = true;
        let i = 0;
        if (Number(busqueda).toString() !== 'NaN') {
          while (Bool) {
            const name = data[i].cc;
            if (name.startsWith(busqueda)) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
              console.log('termine');
            }
          }
        } else {
          while (Bool) {
            const name = data[i].name.toUpperCase();
            if (name.startsWith(busqueda.toLocaleUpperCase())) {
              if (view === 'pendientes') {
                if (!data[i].asistencia) {
                  array.push(data[i]);
                }
              } else if (view === 'registrados') {
                if (data[i].asistencia) {
                  array.push(data[i]);
                }
              } else {
                array.push(data[i]);
              }
            }
            i++;
            if (i === data.length || array.length >= 20) {
              Bool = false;
              console.log('termine');
            }
          }
        }
        this.personas = array;
      },
      10);
  }

  viewDate(cedula: string) {
    let dateFinal = '';
    // tslint:disable-next-line:only-arrow-functions
    this.allData.forEach(function(el) {
      if (el.cc === cedula) {
        console.log(el);
        console.log(el.fecha);
        const date = new Date(el.fecha);
        let hora = date.getHours().toString();
        let minutos = date.getMinutes().toString();
        if (date.getHours() <= 9) {
          hora = '0' + date.getHours().toString();
        }
        if (date.getMinutes() <= 9) {
          minutos = '0' + date.getMinutes().toString();
        }
        dateFinal = date.toLocaleDateString() + ' ' + hora + ':' + minutos;
      }
    });
    this.dateRevisar = dateFinal;
  }

  pedirAyuda() {
    console.log('aiuda');
    this.socketPublic.emit('pedirAyuda');
    alert('Se envio tu petición');
  }

}
