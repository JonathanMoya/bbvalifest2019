import {Component, OnInit} from '@angular/core';
import {SocketPublicService} from '../../services/socket-public.service';
// import {SocketAdminService} from '../../services/socket-admin.service';
import {Observable} from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
declare var jQuery:any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  personas$: Observable<any>;
  personas: any = '';
  asistenciaNum = 0;

  equipo = "";
  impresora = false;

  estaIngresado = false;
  noExisteEquipo = false;

  constructor(private socketPublic: SocketPublicService, @Inject(LOCAL_STORAGE) private storage: StorageService) {
    this.verificarRegistroEquipo();
  }

  verificarRegistroEquipo(){
    if(this.storage.get("equipo")){
      this.socketPublic.emit('registrarEquipo',this.storage.get("equipo"));      
      this.equipo = this.storage.get("equipo").equipo;
      this.impresora = this.storage.get("equipo").impresora;
      this.estaIngresado = true;
    }
  }

  ngOnInit() {
    this.socketPublic.emit('message', 'Hola');
    this.personas$ = this.socketPublic.fromEvent('personasAll');

    this.personas$.subscribe(data => {
      let num = 0;
      this.personas = data;
      // tslint:disable-next-line:only-arrow-functions
      this.asistenciaNum = 0;
      data.forEach(function(ele) {
        if (ele.asistencia) {
          num++;
        }
      });
      this.asistenciaNum = num;
    });

    this.socketPublic.fromEvent('conection').subscribe(data => {
      console.log('LLego la conexion');
      this.verificarRegistroEquipo();
    });

    this.socketPublic.fromEvent('disconnect').subscribe(data => {
      console.log('F por el servidor');
    });
  }

  ingresar(){
    console.log("Quiero Ingresar");
    this.socketPublic.emit('ingresarEquipo',{equipo:this.equipo,impresora:this.impresora})
    this.socketPublic.fromEvent('equipoExiste').subscribe(data => {
      console.log('Equipo Existe');
      this.estaIngresado = false;
      this.noExisteEquipo = true;
    });
    this.socketPublic.fromEvent('equipoIngresado').subscribe(data => {
      console.log('Equipo Ingresado');
      this.estaIngresado = true;
      this.noExisteEquipo = false;
      jQuery('#hostModal').modal('hide');
      this.storage.set("equipo",{equipo:this.equipo,impresora:this.impresora});
    });
  }

  cerrarSesion(){
    this.socketPublic.emit('cerrarSesion',{equipo:this.equipo});
    this.socketPublic.fromEvent('sesionEliminada').subscribe(data => {
      this.storage.remove("equipo");
      this.equipo = "";
      this.impresora = false;
      this.estaIngresado = false;

      this.noExisteEquipo = false;
      jQuery('#hostModalIngresado').modal('hide');
    });
  }

  actualizar(){
    this.socketPublic.emit('actualizarEquipo',{equipo:this.equipo,impresora:this.impresora});
    this.socketPublic.fromEvent('equipoActualizado').subscribe(data => {
      this.storage.set("equipo",{equipo:this.equipo,impresora:this.impresora});
    
      this.estaIngresado = true;
      this.noExisteEquipo = false;
      jQuery('#hostModalIngresado').modal('hide');
    });
  }


}
