import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SocketPublicService extends Socket {

  constructor() {
    super({url: 'http://192.168.0.3:8080/public', options: {}});
  }

}
