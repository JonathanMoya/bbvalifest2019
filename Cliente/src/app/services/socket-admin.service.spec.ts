import { TestBed } from '@angular/core/testing';

import { SocketAdminService } from './socket-admin.service';

describe('SocketAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketAdminService = TestBed.get(SocketAdminService);
    expect(service).toBeTruthy();
  });
});
