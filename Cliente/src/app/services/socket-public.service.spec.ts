import { TestBed } from '@angular/core/testing';

import { SocketPublicService } from './socket-public.service';

describe('SocketPublicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketPublicService = TestBed.get(SocketPublicService);
    expect(service).toBeTruthy();
  });
});
