import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SocketAdminService extends Socket {

  constructor() {
    super({url: 'http://localhost:8080/admin', options: {}});
  }
}
